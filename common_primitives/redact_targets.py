import copy
import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

Inputs = container.List
Outputs = container.List


class Hyperparams(hyperparams.Hyperparams):
    pass


# TODO: Make clear the assumption that both container type (List) and Datasets should have metadata.
#       Primitive is modifying metadata of Datasets, while there is officially no reason for them
#       to really have metadata: metadata is stored available on the input container type, not
#       values inside it.
class RedactTargetsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which takes as an input a list of ``Dataset`` objects and redacts values of all target
    columns (semantic type ``https://metadata.datadrivendiscovery.org/types/TrueTarget``).

    Redaction is done by setting all values in a redacted column to an empty string.

    It operates only on DataFrame resources inside datasets.
    """

    __author__ = 'Mingjie Sun <sunmj15@gmail.com>'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '744c4090-e2f6-489e-8efc-8b1e051bfad6',
            'version': '0.1.0',
            'name': "Redact targets for evaluation",
            'python_path': 'd3m.primitives.evaluation.RedactTargets',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:sunmj15@gmail.com',
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                 metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.EVALUATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        output_datasets = container.List()

        for dataset in inputs:
            resources = {}
            metadata = dataset.metadata

            for resource_id, resource in dataset.items():
                if not isinstance(resource, container.DataFrame):
                    resources[resource_id] = resource
                    continue

                target_columns = self._get_target_columns(metadata, (resource_id,))

                if not target_columns:
                    resources[resource_id] = resource
                    continue

                resource = copy.copy(resource)

                for column_index in target_columns:
                    column_metadata = dataset.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, column_index))
                    if 'structural_type' in column_metadata and issubclass(column_metadata['structural_type'], str):
                        resource.iloc[:, column_index] = ''
                    else:
                        raise TypeError("Primitive can operate only on columns with structural type \"str\", not \"{type}\".".format(
                            type=column_metadata.get('structural_type', None),
                        ))

                    metadata = metadata.add_semantic_type((resource_id, metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/RedactedTarget', source=self)
                    metadata = metadata.add_semantic_type((resource_id, metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/MissingData', source=self)

                resources[resource_id] = resource

            dataset = container.Dataset(resources, metadata, source=self)

            output_datasets.append(dataset)

        output_datasets.metadata = inputs.metadata.clear({
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.List,
            'dimension': {
                'length': len(output_datasets),
            },
        }, for_value=output_datasets, generate_metadata=False, source=self)

        # We update metadata based on metadata of each dataset.
        # TODO: In the future this might be done automatically by generate_metadata.
        #       See: https://gitlab.com/datadrivendiscovery/d3m/issues/119
        for index, dataset in enumerate(output_datasets):
            output_datasets.metadata = utils.copy_metadata(dataset.metadata, output_datasets.metadata, (), (index,), source=self)

        return base.CallResult(output_datasets)

    @classmethod
    def _get_target_columns(cls, inputs_metadata: metadata_base.DataMetadata, at: metadata_base.Selector) -> typing.Sequence[int]:
        return utils.list_columns_with_semantic_types(inputs_metadata, ['https://metadata.datadrivendiscovery.org/types/TrueTarget'], at=at)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        input_datasets_length = inputs_metadata.query(()).get('dimension', {}).get('length', 0)

        outputs_metadata = inputs_metadata.clear({
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.List,
            'dimension': {
                'length': input_datasets_length,
            },
        }, generate_metadata=False, source=cls)

        for index in range(input_datasets_length):
            outputs_metadata = utils.copy_metadata(inputs_metadata, outputs_metadata, (index,), (index,), source=cls)

            for resource_id in inputs_metadata.get_elements((index,)):
                if resource_id is metadata_base.ALL_ELEMENTS:
                    continue

                resource_metadata = inputs_metadata.query((index, resource_id))

                if not issubclass(resource_metadata['structural_type'], container.DataFrame):
                    continue

                target_columns = cls._get_target_columns(inputs_metadata, (index, resource_id))

                if not target_columns:
                    continue

                for column_index in target_columns:
                    column_metadata = inputs_metadata.query((index, resource_id, metadata_base.ALL_ELEMENTS, column_index))
                    if 'structural_type' in column_metadata and issubclass(column_metadata['structural_type'], str):
                        pass
                    else:
                        raise TypeError("Primitive can operate only on columns with structural type \"str\", not \"{type}\".".format(
                            type=column_metadata.get('structural_type', None),
                        ))

                    outputs_metadata = outputs_metadata.add_semantic_type(
                        (index, resource_id, metadata_base.ALL_ELEMENTS, column_index),
                        'https://metadata.datadrivendiscovery.org/types/RedactedTarget', source=cls,
                    )
                    outputs_metadata = outputs_metadata.add_semantic_type(
                        (index, resource_id, metadata_base.ALL_ELEMENTS, column_index),
                        'https://metadata.datadrivendiscovery.org/types/MissingData', source=cls,
                    )

        return outputs_metadata
